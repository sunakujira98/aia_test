const express = require("express");
const app = express();
const request = require("request");
const port = 3001;

app.get("/", (req, res) => {
  let url =
    "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=782f256cab3f792783b80e2b30e10569&tags=cats&format=json&nojsoncallback=1&per_page=20&page=1&tags=cats";

  req.pipe(request(url)).pipe(res);
});

app.get("/:page-:tags", (req, res) => {
  let url =
    "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=782f256cab3f792783b80e2b30e10569&tags=cats&format=json&nojsoncallback=1&per_page=20&page=" +
    req.params.page +
    "&tags=" +
    req.params.tags;
  req.pipe(request(url)).pipe(res);
});

app.get("/search/:tags", (req, res) => {
  let url =
    "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=782f256cab3f792783b80e2b30e10569&tags=cats&format=json&nojsoncallback=1&per_page=20&tags=" +
    req.params.tags;
  req.pipe(request(url)).pipe(res);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
